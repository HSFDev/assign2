<!Doctype html>
<html>
<head>
		<title>Register</title>
        <link type="text/css" rel="stylesheet" href="css/style.css" />
        <link href="css/m-styles.min.css" rel="stylesheet">
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
    <nav>
        <div id="topmenu">
                <a href="index.php">Home</a> 
                <a href="explore.php">Explore</a> 
                <a href="#">Shops</a> 
                <a href="#">Community</a> 
                <a href="#">Games</a> 
        </div>   
    </nav>
    <?php if (isset ($_SESSION ['username'])) : ?>
    <a href="logout.php" id="logoutbtn">Logout</a> 
    <?php endif; ?>
    <header>
    </header>
        
    <main id="shop">           
        <div id="categories">
            <h2>Categories</h2>
            <ul>
                <li><a href="#">All</a></li>
                <li><a href="#">Items 1</a></li>
                <li><a href="#">Items 2</a></li>
                <li><a href="#">Time</a></li>
            </ul>
        </div>
          
        <table id="main">
            <tr>
                <td>
                    <img alt="item1" src="img/item1.png" />
                    <h4>Sword</h4>
                    <p>1$</p>
                    <a href="#" id="buy" ><p >Buy</p></a>
                </td>
               <td>
                    <img alt="item1" src="img/item1.png" />
                    <h4>Sword</h4>
                    <p>1$</p>
                    <a href="#" id="buy" ><p >Buy</p></a>
                </td>
                <td>
                    <img alt="item1" src="img/item1.png" />
                    <h4>Sword</h4>
                    <p>1$</p>
                    <a href="#" id="buy" ><p >Buy</p></a>
                </td>
            </tr>
            <tr>
                <td>
                    <img alt="item1" src="img/item1.png" />
                    <h4>Sword</h4>
                    <p>1$</p>
                    <a href="#" id="buy" ><p >Buy</p></a>
                </td>
                <td>
                    <img alt="item1" src="img/item1.png" />
                    <h4>Sword</h4>
                    <p>1$</p>
                    <a href="#" id="buy" ><p >Buy</p></a>
                </td>
               <td>
                    <img alt="item1" src="img/item1.png" />
                    <h4>Sword</h4>
                    <p>1$</p>
                    <a href="#" id="buy" ><p >Buy</p></a>
                </td>
            </tr>
        </table>
    </main>
    <footer>
        
    </footer>
        
</body>
</html>