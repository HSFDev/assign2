<?php 
session_start ();
?>
<!Doctype html>
<html>
<head>
		<title>Player Page</title>
        <link type="text/css" rel="stylesheet" href="../css/style.css" />
        
</head>
<body>
    <nav>
        <div id="topmenu">
                <a href="../index.php">Home</a> 
                <a href="../explore.php">Explore</a> 
                <a href="#">Shops</a> 
                <a href="#">Community</a> 
                <a href="#">Games</a> 
        </div>   
    </nav>
    <?php if (isset ($_SESSION ['username'])) : ?>
    <a href="../logout.php" id="logoutbtn">Logout</a> 
    <?php endif; ?>
  
        
    <main>           
        <!-- Sidebar -->
        <section class="sidebar">
        
             <div id="topsidebar">
            <p id="charactername">Character Name</p>
            <p id="playerlevel">Lvl.1</p>
            </div>
            
            <img id="character" alt="character" src="../img/character2.png.png"/>
            
            <img id="stats" alt="stats" src="../img/hp-min.png" />
            
            <div id="items">
                <img id="item1" alt="item1" src="../img/item1.png" />
                <img id="item2" alt="item2" src="../img/Item2.png" />
            </div>
            
            <div id="time">
                <img id="clock" alt="clock" src="../img/clock.png" />
                <p id="mins">3,000 min</p>
            </div>
        </section>

        <section class="main">
            <!-- Notices -->
            <div id="box1">
                <div id="headbox1">
                    <h3 class="boxtitle">Notices</h3>
                </div>
                   
                <ul>
                    <li>News 1</li>
                    <li>News 2</li>
                    <li>News 3</li>
                </ul>
            </div>
            
            <div id="mainbox2">
                <div id="box21">
                <div id="headbox21">
                    <h3 class="boxtitle">Quests</h3>
                </div>
                </div>
            
                <div id="box22">
                <div id="headbox22">
                    <h3 class="boxtitle">Mailbox</h3>
                </div>
                </div>
            </div>
            
            <div id="mainbox3">
                <a href="#"><div class="boxbtn" id="boxbtn1"><h3 class="boxtitle">Inventory</h3></div></a>
                <a href="acc-settings.php"><div class="boxbtn" id="boxbtn2"><h3 class="boxtitle">Account Settings</h3></div></a>
            </div>
            <div id="mainbox4">
                <a href="#"><div class="boxbtn" id="boxbtn3"><h3 class="boxtitle">Avatar</h3></div></a>
                <a href="#"><div class="boxbtn" id="boxbtn4"><h3 class="boxtitle">Friends</h3></div></a>
            </div>
        </section>
    </main>
    <footer>
        
    </footer>
        
</body>
</html>