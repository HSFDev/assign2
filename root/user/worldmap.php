<!Doctype html>
<html>
<head>
		<title>World Map</title>
        <link type="text/css" rel="stylesheet" href="../css/style.css" />
        
</head>
<body>
    <nav>
        <div id="topmenu">
                <a href="../index.php">Home</a> 
                <a href="../explore.php">Explore</a> 
                <a href="#">Shops</a> 
                <a href="#">Community</a> 
                <a href="#">Games</a> 
        </div>   
    </nav>
    <?php if (isset ($_SESSION ['username'])) : ?>
    <a href="../logout.php" id="logoutbtn">Logout</a> 
    <?php endif; ?>
  
        
    <main id="worldmap">           
        <!-- Sidebar -->
        <section class="sidebar">
        
             <div id="topsidebar">
            <p id="charactername">Character Name</p>
            <p id="playerlevel">Lvl.1</p>
            </div>
            
            <img id="character" alt="character" src="../img/character2.png.png"/>
            
            <img id="stats" alt="stats" src="../img/hp-min.png" />
            
            <div id="items">
                <img id="item1" alt="item1" src="../img/item1.png" />
                <img id="item2" alt="item2" src="../img/item2.png" />
            </div>
            
            <div id="time">
                <img id="clock" alt="clock" src="../img/clock.png" />
                <p id="mins">3,000 min</p>
            </div>
        </section>

        <section class="main">
            <!-- Map -->
            <section id="map"></section>
        
        </section>
    </main>
    <footer>
        
    </footer>
        
</body>
</html>