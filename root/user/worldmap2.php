<!Doctype html>
<html>
<head>
		<title>World Map 2</title>
        <link type="text/css" rel="stylesheet" href="../css/style.css" />
        
</head>
<body>
    <nav>
        <div id="topmenu">
                <a href="../index.php">Home</a> 
                <a href="../explore.php">Explore</a> 
                <a href="#">Shops</a> 
                <a href="#">Community</a> 
                <a href="#">Games</a> 
        </div>   
    </nav>
    <?php if (isset ($_SESSION ['username'])) : ?>
    <a href="logout.php" id="logoutbtn">Logout</a> 
    <?php endif; ?>
  
        
    <main id="worldmap2">   
        
       <img src="../img/character.png" alt="character" />
        
        <div id="userinfo">
            
        </div>
        
        <nav>
            
            
        </nav>
        
        <section id="map">
            
        </section>
        
    </main>
    <footer>
        
    </footer>
        
</body>
</html>