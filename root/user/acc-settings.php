<?php
    session_start();
    require('includes/connect.inc.php');

    //If Submit Pressed 
    if( isset( $_POST[ 'submit' ] ) ){    

                    
            if( ( (strcmp( $_POST[ 'pass' ], $user[ 'password' ] ) == 0)  ||  (strcmp( $_POST[ 'email' ], $user[ 'email' ] ) == 0) ) && ( ( $_POST['npass'] === $_POST['ncpass'] ) || ( $_POST['nemail'] === $_POST['ncemail'] ) ) ){
                    
                $query = "UPDATE users (password,email) VALUES ('$_POST[npass]','$_POST[nemail]')";
                    
                $result = mysqli_query( $db, $query ) 
                or die( mysqli_error( $db ) );
                    }
                }         
?>
<!Doctype html>
<html>
<head>
		<title>Player Page</title>
        <link type="text/css" rel="stylesheet" href="../css/style.css" />
        
</head>
<body>
    <nav>
        <div id="topmenu">
                <a href="../index.php">Home</a> 
                <a href="../explore.php">Explore</a> 
                <a href="#">Shops</a> 
                <a href="#">Community</a> 
                <a href="#">Games</a> 
        </div>   
    </nav>
    <?php if (isset ($_SESSION ['username'])) : ?>
    <a href="../logout.php" id="logoutbtn">Logout</a> 
    <?php endif; ?>
  
        
    <main id="acc-settings">           
        <div id="emaildiv">
            <form method="post" action="<?php echo $_SERVER[ 'PHP_SELF' ] ?>" >
            <ul>
                <li>
                    <label for="email">Email</label>
                    <input id="email" type="email" name="email" placeholder="Email" />
                </li>
                <li>
                    <label for="nemail">New Email</label>
                    <input id="nemail" type="email" name="nemail" placeholder="New Email"/>
                </li>
                <li>
                    <label for="cnewmail">Confirm new email</label>
                    <input id="cnemail" type="email" name="cnemail" placeholder="Confirm New Email" />
                </li>
                <li>
                    <input type="submit" value="submit" />
                </li>
            </ul>
            </form>
        </div>
        <div id="passdiv">
            <form method="post" action="<?php echo $_SERVER[ 'PHP_SELF' ] ?>" >
            <ul>
                <li>
                    <label for="pass">Password</label>
                    <input id="pass" type="password" name="pass" placeholder="Password" />
                </li>
                <li>
                    <label for="npass">New Password</label>
                    <input id="npass" type="password" name="npass" placeholder="New Password" />
                </li>
                <li>
                    <label for="cnpass">Confirm new Password</label>
                    <input id="cnpass" type="password" name="cnpass" placeholder="Confirm New Password" />
                </li>
                <li>
                    <input type="submit" value="submit" />
                </li>
            </ul>
            </form>
        </div>
    </main>
    <footer>
        
    </footer>
        
</body>
</html>