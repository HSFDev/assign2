<?php
    require('includes/connect.inc.php');

    $errors = array(); 

    //If Submit Pressed 
    if( isset( $_POST[ 'submit' ] ) ){    
        //and no errors
        if( count( $errors ) == 0 ){
                //Set username selection query 
                $sql = "SELECT * FROM users 
                        WHERE username = '$_POST[username]'";
                //Execute query
                $result = mysqli_query( $db, $sql ) 
                or die( mysqli_error( $db ) );
            
                //If Username exist check if entered password match DB password
                if( mysqli_num_rows( $result ) > 0 ){
                    $user = mysqli_fetch_assoc( $result );
                    
                    //Check if password match
                    if( strcmp( $_POST[ 'pass' ], $user[ 'password' ] ) == 0 ){
                        //If password match save player data in $_SESSION for later use                   
                        $_SESSION[ 'username' ]     = $user[ 'username' ];
                        $_SESSION[ 'email' ]        = $user[ 'email' ];
                        $_SESSION[ 'user-id' ]      = $user[ 'id' ];
                        //Redirect player
                        header( 'Location: index.php' );
                    } else {
                        $errors[ 'login' ] = '<p class="error">The playername or Password entered was incorrect.</p>';
                            }
                } 
                else 
                 {
               
                    $errors[ 'login' ] = '<p class="error">The playername entered does not exist.</p>';
                 }   
            }
        }
?>
<!doctype html>
<html>
    <head>
        <title>Login</title>
    </head>
    
    <body>
    
        <form method="post" action="<?php echo $_SERVER[ 'PHP_SELF' ] ?>">
            <ul>
                <li>
                    <label for="username">Login</label>
				    <input id="name" name="username" type="text" size="40" placeholder="Username" />
                </li>
                <li>
                    <label for="pass">Password</label>
                    <input id="pass" name="pass" type="password" size="60" placeholder="Password"/>
                </li>
                <li>
                    <input type="submit" name="submit" value="submit" />
                </li>
            </ul>    
        </form>
    </body>
</html>