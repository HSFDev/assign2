<?php
    require('includes/connect.inc.php');

    $errors = array(); 

    //If Submit Pressed 
    if( isset( $_POST[ 'submit' ] ) ){    
        //and no errors
        if( count( $errors ) == 0 ){
                //Set username selection query 
                $sql = "SELECT * FROM users 
                        WHERE username = '$_POST[username]'";
                //Execute query
                $result = mysqli_query( $db, $sql ) 
                or die( mysqli_error( $db ) );
            
                //If Username exist check if entered password match DB password
                if( mysqli_num_rows( $result ) > 0 ){
                    
                    $user = mysqli_fetch_assoc( $result );
                    
                    //Check if password match
                    if( strcmp( $_POST[ 'pass' ], $user[ 'password' ] ) == 0 ){
                        //If password match save player data in $_SESSION for later use                   
                        $_SESSION[ 'username' ]     = $user[ 'username' ];
                        $_SESSION[ 'email' ]        = $user[ 'email' ];
                        $_SESSION[ 'user-id' ]      = $user[ 'id' ];
                        //Redirect player
                        header( 'Location: user/index.php' );
                    } else {
                        $errors[ 'login' ] = '<p class="error">The playername or Password entered was incorrect.</p>';
                            }
                } else {
               
                    $errors[ 'login' ] = '<p class="error">The playername entered does not exist.</p>';
                        }   
            }
        }
?>
<!Doctype html>
<html>
<head>
		<title>Register</title>
        <link type="text/css" rel="stylesheet" href="css/style.css" />
        <link href="css/m-styles.min.css" rel="stylesheet">
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
    <nav>
        <div id="topmenu">
                <a href="index.php">Home</a> 
                <a href="explore.php">Explore</a> 
                <a href="#">Shops</a> 
                <a href="#">Community</a> 
                <a href="#">Games</a> 
        </div>   
    </nav>
    <?php if (isset ($_SESSION ['username'])) : ?>
    <a href="logout.php" id="logoutbtn">Logout</a> 
    <?php endif; ?>
    <header>
    </header>
        
    <main>           
        <div class="col23 fl" id="boxhome">
            
            <p>Welcome ...</p>
            
            <a href="#login_form"><div id="joinbtn">Join Now</div></a>
            
        </div>
            
        <div class="col13 fr">
            <?php if (!isset ($_SESSION ['username'])) : ?>
            <form method="post" action="<?php echo $_SERVER[ 'PHP_SELF' ] ?>">
                <ul>
                        
                    <li>
				        <input id="name" name="username" type="text" size="30" placeholder="Username" />
                    </li>
                        
                    <li>
                        <input id="pass" name="pass" type="password" size="30" placeholder="Password"/>
                    </li>
                        
                    <li>
                        <input type="submit" name="submit" value="Login" />
                    </li>
                        
                </ul>    
            </form>
            
            <?php else : ?>
                
            <p>Welcome Player</p>
            
            <?php endif; ?>
        </div>
        
        <div class="col23 fl" >
            <p>News &amp; Updates</p> 
        </div>
            
        <div class="col13 fr" id="nobg">
            <a href="#login_form" id="login_pop"><div id="registerbtn"><span>Register Now</span></div></a>
        </div>
        
        <a href="#x" class="overlay" id="login_form"></a>
        <div class="popup">
            
            <?php 
    require('includes/connect.inc.php');

        //If Submit Pressed :
        if( isset( $_POST[ 'registerbtn' ] ) ){
            //1-Check all inputs filled
            if (  isset($_POST['username']) && isset($_POST['email']) && isset( $_POST['pass1']) )
            {
                //2-Check if user entred Password correctly :  
                if ( $_POST['pass1'] === $_POST['pass2'] ) 
                {    
                    //2:1-If Pass1 match Pass2 Register user in DB 
                    
                    $sql = "INSERT INTO users (username,password,email) VALUES ('$_POST[username]','$_POST[pass1]','$_POST[email]')";
                    $result = mysqli_query($db,$sql)
                    or die(mysqli_error( $db ));
        
                    //Successful Registration
                    print "<div class='success'>registration successful</h1>";
                    print "<a href='index.php'>go back to Home page</a>";
                }
                else print "Passwords don't match";
            }
            else print "Please Fill all fields";
        }
?>
            
            
        <h2 id="registernowpop">REGISTER NOW!</h2>
        <form method="post" action="<?php echo $_SERVER[ 'PHP_SELF' ] ?>">

				<ul>
					<li>
						<label for="username">Username</label>
						<input id="name" name="username" type="text" size="40" placeholder="Username" />
					</li>	 

                    <li>
                        <label for="email">Email</label>
                        <input id="email" type="email" name="email" size="150" placeholder="email" />
                    </li>
                    
                    <li>
                        <label for="pass1">Password</label>
                        <input id="pass1" name="pass1" type="password" size="60" placeholder="Password"/>
                    </li> 
                    <li>
                        <label for="pass2">Re-enter Password</label>
                        <input id="pass2" name="pass2" type="password" size="60" placeholder="Confirm Password"/>
                    </li> 
                    
				    <li>
                        <div class="g-recaptcha" data-sitekey="6LeOYAgTAAAAABb2IMOgqoBdA1rTe3LVYOGwRxVd"></div>
                        <input type="submit" name="registerbtn" value="SUBMIT"/>
                    </li>
                </ul>
                

			</form>
   <a class="close" href="#close"></a>
</div>  
			
    </main>
    <footer>
        
    </footer>
        
</body>
</html>