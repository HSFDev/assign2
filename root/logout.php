<?php

    session_start();
    
    //Set all Player data to null
    $_SESSION[ 'username' ]     = null;
    $_SESSION[ 'email' ]        = null;
    $_SESSION[ 'user-id' ]      = null;

    //Unset all Player data (Just to make sure User is totally logged out)
    unset( $_SESSION[ 'username' ] );
    unset( $_SESSION[ 'email' ] );
    unset( $_SESSION[ 'user-id' ] );

    //Redirect Player to Homepage
    header( 'Location: index.php' );